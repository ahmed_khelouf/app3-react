import React from 'react';
import { StyleSheet, Text, View, Image, ScrollView } from 'react-native';

const DetailPageComponent = ({ route }) => {
    const { cocktail } = route.params;

    return (
        <ScrollView contentContainerStyle={styles.container}>
            <Image style={styles.image} source={{ uri: cocktail.strDrinkThumb }} />
            <Text style={styles.title}>{cocktail.strDrink}</Text>
            <Text style={styles.subtitle}>Ingrédients :</Text>
            {Object.keys(cocktail)
                .filter(key => key.startsWith('strIngredient') && cocktail[key])
                .map((key, index) => (
                    <Text key={index} style={styles.ingredient}>
                        {cocktail[key]} {cocktail[`strMeasure${key.slice(13)}`]}
                    </Text>
                ))}
            <Text style={styles.subtitle}>Instructions :</Text>
            <Text style={styles.instructions}>{cocktail.strInstructions}</Text>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f5f5f5',
        padding: 20,
        alignItems: 'center',
    },
    image: {
        width: '100%',
        height: 300,
        borderRadius: 10,
        marginBottom: 20,
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 10,
        textAlign: 'center',
        color: '#333',
    },
    subtitle: {
        fontSize: 20,
        fontWeight: '600',
        marginTop: 20,
        marginBottom: 10,
        color: '#666',
    },
    ingredient: {
        fontSize: 16,
        color: '#555',
    },
    instructions: {
        fontSize: 16,
        color: '#555',
        marginTop: 10,
    },
});

export default DetailPageComponent;
