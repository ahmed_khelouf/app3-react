import { StyleSheet, Text, View, Image, ScrollView, TouchableOpacity } from 'react-native';
import React from 'react';

const HomepageComponent = ({ navigation, cocktails }) => {
    if (!cocktails) {
        return (
            <View style={styles.container}>
                <Text style={styles.paragraph}>Loading...</Text>
            </View>
        );
    }
    return (
        <View style={styles.container}>
            <ScrollView contentContainerStyle={styles.scrollViewContent}>
                <Text style={styles.title}>Liste Cocktail :</Text>
                {cocktails.map((c, index) => (
                    <TouchableOpacity
                        key={index}
                        style={styles.cocktailContainer}
                        onPress={() => navigation.navigate('Details', { cocktail: c })}
                    >
                        <Image
                            style={styles.cocktailImage}
                            source={{ uri: c.strDrinkThumb }}
                        />
                        <Text style={styles.cocktailName}>{c.strDrink}</Text>
                    </TouchableOpacity>
                ))}
            </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f5f5f5',
    },
    scrollViewContent: {
        alignItems: 'center',
        paddingVertical: 20,
    },
    paragraph: {
        margin: 24,
        fontSize: 18,
        textAlign: 'center',
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        textAlign: 'center',
        marginBottom: 20,
        color: '#333',
    },
    cocktailContainer: {
        backgroundColor: '#fff',
        borderRadius: 10,
        padding: 20,
        marginBottom: 20,
        width: '90%',
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.1,
        shadowRadius: 5,
        elevation: 5,
    },
    cocktailImage: {
        width: 150,
        height: 150,
        borderRadius: 75,
        marginBottom: 10,
    },
    cocktailName: {
        fontSize: 20,
        fontWeight: '600',
        textAlign: 'center',
        color: '#555',
    },
});

export default HomepageComponent;
