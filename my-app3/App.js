import React, { useEffect, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { ActivityIndicator, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import HomepageComponent from './components/Homepage';
import DetailPageComponent from './components/Detail';
import TestComponent from './components/Test';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const CocktailStack = ({ cocktails }) => (
  <Stack.Navigator>
    <Stack.Screen name="Home">
      {(props) => <HomepageComponent {...props} cocktails={cocktails} />}
    </Stack.Screen>
    <Stack.Screen name="Details" component={DetailPageComponent} />
  </Stack.Navigator>
);

const App = () => {
  const [cocktails, setCocktails] = useState([]);
  const [loading, setLoading] = useState(true);

  const getCocktails = async () => {
    try {
      const letters = 'abcdefghijklmnopqrstuvwxyz'.split('');
      let allCocktails = [];
      for (let letter of letters) {
        const response = await fetch(`https://www.thecocktaildb.com/api/json/v1/1/search.php?f=${letter}`);
        const data = await response.json();
        if (data.drinks) {
          allCocktails = [...allCocktails, ...data.drinks];
        }
      }
      setCocktails(allCocktails);
      setLoading(false);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    getCocktails();
  }, []);

  if (loading) {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    );
  }

  return (
    <NavigationContainer>
      <Tab.Navigator initialRouteName="Home">
        <Tab.Screen
          name="Home"
          options={{
            tabBarIcon: ({ color, size }) => (
              <Icon name="home" color={color} size={size} />
            ),
          }}
        >
          {() => <CocktailStack cocktails={cocktails} />}
        </Tab.Screen>
        <Tab.Screen
          name="Profile"
          options={{
            title: 'Test',
            tabBarIcon: ({ color, size }) => (
              <Icon name="user" color={color} size={size} />
            ),
          }}
        >
          {() => <TestComponent />}
        </Tab.Screen>
      </Tab.Navigator>
    </NavigationContainer>
  );
};

export default App;
